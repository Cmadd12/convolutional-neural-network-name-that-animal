### create your virtual python environment
```
virtualenv -p python3.6 py_36_env
source py_36_env/bin/activate
pip install ipykernel
python -m ipykernel install --user --name=py_36_env
```

### if you end terminal session activate your virtual enviornment again
```
source py_36_env/bin/activate
```

### install tensorflow 1.4.0
```
pip3 install --upgrade tensorflow==1.4
```

### verify tensorflow version is 1.4.0
```
python
>>>import tensorflow as tf
>>>tf.__version__
```

### create a simple tensorflow program inside the python session
```
>>> import tensorflow as tf
>>> hello = tf.constant('Hello, TensorFlow!')
>>> sess = tf.Session()
>>> print(sess.run(hello))
>>>exit
```
### import matplot lib to view images in jupyter notebook
```
python3 -mpip install matplotlib
```

### import numpy 
```
python -m pip install numpy
```

### import opencv2 that will allow for resizing files
```
pip install opencv-python
```

### launch the jupyter notebook server
```
$ jupyter notebook
```

### stop the jupyter notebook server
```
Ctrl+C 
```