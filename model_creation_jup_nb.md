Import dependencies and data

```python
import tensorflow as tf
from tensorflow.python.keras.models import Sequential
from tensorflow.python.keras.layers import Dense, Dropout, Activation, Flatten, Conv2D, MaxPooling2D
import pickle

X = pickle.load(open('X.pickle', 'rb'))
y = pickle.load(open('y.pickle', 'rb'))

X = X/255.0
```

Model creation - 3 layers

```python
#add layers as an array model = Sequential([Flatten(), Dense(), Activation()])
model = Sequential()#First layer of CNN

model.add(Conv2D(64, (3,3), input_shape = X.shape[1:]))
model.add(Activation('relu'))
model.add(MaxPooling2D(pool_size=(2,2)))

model.add(Conv2D(64, (3,3), input_shape = X.shape[1:])) #Second layer of CNN
model.add(Activation('relu'))
model.add(MaxPooling2D(pool_size=(2,2)))

model.add(Flatten())#Third layer of CNN
model.add(Activation('relu'))
model.add(Dense(64))#need to flatten here because flatten uses a 1 dimentional data set.

model.add(Dense(1))
model.add(Activation('sigmoid'))

model.compile(loss="binary_crossentropy",
             optimizer='adam',
             metrics=['accuracy'])

model.fit(X, y, batch_size=32, epochs=10, validation_split=0.1)#this is how many images you want to pass through the network.
```

Epoch results

```python
# Train on 22451 samples, validate on 2495 samples
# Epoch 1/10
# 22451/22451 [==============================] - 168s - loss: 0.6355 - acc: 0.6405 - val_loss: 0.5800 - val_acc: 0.7010
# Epoch 2/10
# 22451/22451 [==============================] - 178s - loss: 0.5277 - acc: 0.7412 - val_loss: 0.5048 - val_acc: 0.7599
# Epoch 3/10
# 22451/22451 [==============================] - 167s - loss: 0.4740 - acc: 0.7793 - val_loss: 0.4703 - val_acc: 0.7764
# Epoch 4/10
# 22451/22451 [==============================] - 162s - loss: 0.4347 - acc: 0.7989 - val_loss: 0.4610 - val_acc: 0.7980
# Epoch 5/10
# 22451/22451 [==============================] - 162s - loss: 0.4022 - acc: 0.8166 - val_loss: 0.4601 - val_acc: 0.7952
# Epoch 6/10
# 22451/22451 [==============================] - 163s - loss: 0.3727 - acc: 0.8338 - val_loss: 0.4622 - val_acc: 0.7956
# Epoch 7/10
# 22451/22451 [==============================] - 162s - loss: 0.3463 - acc: 0.8442 - val_loss: 0.4780 - val_acc: 0.7848
# Epoch 8/10
# 22451/22451 [==============================] - 171s - loss: 0.3217 - acc: 0.8603 - val_loss: 0.4901 - val_acc: 0.7888
# Epoch 9/10
# 22451/22451 [==============================] - 173s - loss: 0.3008 - acc: 0.8699 - val_loss: 0.4891 - val_acc: 0.7984
# Epoch 10/10
# 22451/22451 [==============================] - 167s - loss: 0.2762 - acc: 0.8811 - val_loss: 0.5423 - val_acc: 0.7804
```