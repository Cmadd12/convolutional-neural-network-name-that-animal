```python
import numpy as np
import matplotlib.pyplot as plt
import os
import cv2

DATADIR = '/Users/maddco/documents/microsoft_data_set'#location you downloaded the data set to from microsoft
CATEGORIES = ['Dog', 'Cat']

for category in CATEGORIES:
    path = os.path.join(DATADIR, category)#this is the path to cats and dogs directory
    for img in os.listdir(path):
        img_array = cv2.imread(os.path.join(path,img), cv2.IMREAD_GRAYSCALE)#comment out to make color scale
        plt.imshow(img_array, cmap = 'gray')
        plt.show()
        break
    break
    
print(img_array.shape)

IMG_SIZE = 70 # change image size and see what happens! Think about how this can affect your network learning

new_array = cv2.resize(img_array, (IMG_SIZE, IMG_SIZE))# use this array to resize your images to ensure accurate trian
plt.imshow(new_array, cmap = 'gray')
plt.show()

training_data = []

def create_training_data():
    for category in CATEGORIES:
        path = os.path.join(DATADIR, category)#this is the path to cats and dogs directory
        class_num = CATEGORIES.index(category)
        for img in os.listdir(path):
            try:
                img_array = cv2.imread(os.path.join(path,img), cv2.IMREAD_GRAYSCALE)#comment out to make color scale
                new_array = cv2.resize(img_array, (IMG_SIZE, IMG_SIZE))
                training_data.append([new_array, class_num])
            except Exception as e:
                pass
create_training_data()

print(len(training_data))

import random

random.shuffle(training_data)

for sample in training_data[:10]:
    print(sample[1]) #1 denotes the classification of 0 & 1 respectively for dog/cat

X = [] #capital x is your feature set. Lower case y is your lables
y = []

for features, label in training_data:
    X.append(features)
    y.append(label)
    
X = np.array(X).reshape(-1, IMG_SIZE, IMG_SIZE, 1)#-1 is a catch all for all of the present features. 1 is for gray

import pickle #lets save our data using pickle

pickle_out = open('X.pickle', 'wb')
pickle.dump(X, pickle_out)
pickle_out.close()

pickle_out = open('y.pickle', 'wb')
pickle.dump(y, pickle_out)
pickle_out.close()

pickle_in = open ('X.pickle', 'rb')
X = pickle.load(pickle_in)
```