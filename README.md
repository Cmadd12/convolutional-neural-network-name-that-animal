This project will allow for anyone to have the ability to use tensorflow's keras API to build a convolutional neural network to classify dog and cat images.

Data requirements:
    Please use the following link to access and download the data download data https://www.microsoft.com/en-us/download/confirmation.aspx?id=54765

CREDITS Thank you to Sentdex and Margaret Maynard-Reid for the inspiration and helpful documentation/videos